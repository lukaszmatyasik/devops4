package datastruct;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class MyUnsortedListTest {
	private UnsortedList<Integer> list;
	private UnsortedList<Integer> listEmpty;
	@Before
	public void init() {
		list = MyUnsortedList.of(1,2,3,4);
		listEmpty = MyUnsortedList.of();
	}

	@Test
	public void testIsEmpty() {
		assertTrue("new list", listEmpty.isEmpty());
		listEmpty.append(1);
		assertFalse("list after one add", listEmpty.isEmpty());
		listEmpty.remove(0);
		assertTrue("list after one add+remove", listEmpty.isEmpty());
	}
	
	@Test
	public void testSize() {
		assertEquals("size of list", 4, list.size());
		list.prepend(0);
		assertEquals("size of list after one add", 5, list.size());
		list.remove(0);
		assertEquals("size of list after one remove", 4, list.size());
	}
	
	@Test
	public void testPrepend() {
		list.prepend(0);
		UnsortedList<Integer> list1 = MyUnsortedList.of(0,1,2,3,4);
		assertEquals("size of list after one prepend", list1, list);
	}
	
	@Test
	public void testAppend() {
		list.append(5);
		UnsortedList<Integer> list1 = MyUnsortedList.of(1,2,3,4,5);
		assertEquals("size of list after one prepend", list1, list);
	}
	
	@Test
	public void testInsert() {
		list.insert(0,2);
		UnsortedList<Integer> list1 = MyUnsortedList.of(1,2,0,3,4);
		assertEquals("size of list after one insert", list1, list);
		list.insert(6,5);
		UnsortedList<Integer> list2 = MyUnsortedList.of(1,2,0,3,4,6);
		assertEquals("size of list after two insert", list2, list);
	}
	
	@Test(expected = IndexOutOfBoundsException.class)
	public void testBadInsert() throws Exception{
		list.insert(0, 5);
		list.insert(0, -1);
	}
	
	@Test
	public void testPop() {
		list.pop();
		UnsortedList<Integer> list1 = MyUnsortedList.of(2,3,4);
		assertEquals("size of list after one pop", list1, list);
	}
	
	@Test(expected = EmptyListException.class)
	public void testBadPop() throws Exception{
		listEmpty.pop();
	}
	
	@Test
	public void testPopLast() {
		list.popLast();
		UnsortedList<Integer> list1 = MyUnsortedList.of(1,2,3);
		assertEquals("size of list after one pop last", list1, list);
	}
	
	@Test(expected = EmptyListException.class)
	public void testBadPopLast() throws Exception{
		listEmpty.popLast();
	}
	
	@Test
	public void testRemove() {
		list.remove(2);
		UnsortedList<Integer> list1 = MyUnsortedList.of(1,2,4);
		assertEquals("size of list after one remove", list1, list);
		UnsortedList<Integer> list2 = MyUnsortedList.of(1,2);
		list.remove(2);
		assertEquals("size of list after two remove", list2, list);
	}
	
	@Test(expected = IndexOutOfBoundsException.class)
	public void testBadRemove() throws Exception{
		list.remove(5);
		list.remove(-1);
	}

}
